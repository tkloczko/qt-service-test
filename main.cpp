#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>

#include <QAndroidService>
#include <QAndroidJniObject>
#include <QtAndroid>

int main(int argc, char *argv[])
{
    if (argc > 1) {
        QAndroidJniObject::callStaticMethod<void>("fr/in/tst/MyTest",
                                                  "log",
                                                  "()V",
                                                  QtAndroid::androidActivity().object());
        QAndroidService app(argc, argv);
        return app.exec();

    } else {
        QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
        QGuiApplication app(argc, argv);
        QQmlApplicationEngine engine;
        engine.rootContext()->setContextProperty("availableStyles", QQuickStyle::availableStyles());
        const QUrl url(QStringLiteral("qrc:/main.qml"));
        engine.load(url);
        QAndroidJniObject::callStaticMethod<void>("fr/in/tst/MyTest",
                                                  "startMyService",
                                                  "(Landroid/content/Context;)V",
                                                  QtAndroid::androidActivity().object());
        return app.exec();
    }
}
