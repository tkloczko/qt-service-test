package fr.in.tst;

import android.content.Context;
import android.content.Intent;
import org.qtproject.qt5.android.bindings.QtService;
import android.util.Log;

class BlueService
{
    public static native void start();
}

public class MyTest extends QtService
{
    public static void startMyService(Context ctx) {
        ctx.startService(new Intent(ctx, MyTest.class));
        Log.i("Service", "Start Service");
    }

    public static void log() {
        Log.i("Service", "MyTest as a service");
    }

    @Override
    public void onCreate()
    {
        Log.d("SERVICE", "ON CREATE CALLED");
        Log.i("Service", "Created");
        super.onCreate();
        Log.i("Service", "Created");
        //BlueService.start();
    }
}

