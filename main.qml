import QtQuick 2.14
import QtQuick.Layouts 1.12
import QtQuick.Window 2.14
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    ColumnLayout {
        Label {
            id: toto
            Layout.fillWidth: true
        }
        TextField {
            Layout.fillWidth: true
            placeholderText : "Type a message"
            onTextChanged: {
                console.log(text);
            }
        }
        Item {
           Layout.fillHeight: true
        }
    }
}
