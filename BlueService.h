// BlueService.h
//

#pragma once

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL Java_fr_inria_test_BlueService_start(JNIEnv *, jobject *);

#ifdef __cplusplus
};
#endif

//
// BlueService.h ends here
